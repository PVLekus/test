package gudsoft.ru.news;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import gudsoft.ru.news.data.Article;

import java.io.IOException;
import java.net.URL;

public class NewsCardActivity extends AppCompatActivity {

    ImageView article_image;
    ImageView img_like;
    ImageView img_like_small;
    ImageView refresh;

    ProgressBar progressBar;

    TextView author_text;
    TextView title_text;
    TextView description_text;
    TextView more;
    TextView likes_count;
    TextView data_text;

    Context context = NewsCardActivity.this;

    Bitmap article_bmp;
    Article article;

    final String BITMAP = "bitmap";
    final String JSON = "json";
    final String ARTICLE = "article";
    final String ID = "id";

    //флаг для async_task
    boolean isAlive = true;

    int id = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_card);

        article_image = (ImageView) findViewById(R.id.article_image);
        img_like = (ImageView) findViewById(R.id.img_like);
        img_like_small = (ImageView) findViewById(R.id.img_like_small);
        refresh = (ImageView) findViewById(R.id.img_refresh);
        refresh.setVisibility(View.INVISIBLE);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        author_text = (TextView) findViewById(R.id.author_text);
        title_text = (TextView) findViewById(R.id.title_text);
        description_text = (TextView) findViewById(R.id.description_text);
        more = (TextView) findViewById(R.id.more_text);
        more.setVisibility(View.GONE);
        likes_count = (TextView) findViewById(R.id.likes_count);
        data_text = (TextView) findViewById(R.id.data_text);

        //делаю проаерку на наличие сохраненного состояния и выдергиваю из него
        //изображение и данные статьи + id статьи в листе активити родителя
        //если нет сохр состояния, то получаю все из интента и вызываю метод заполнения данных

        if (savedInstanceState != null) {

            article_bmp = savedInstanceState.getParcelable(BITMAP);
            article = new Article(savedInstanceState.getString(JSON));
            id = savedInstanceState.getInt(ID);

            fillCard();

        } else {

            Intent intent = getIntent();
            article = new Article(intent.getStringExtra(ARTICLE));
            id = intent.getIntExtra(ID, -1);

            Log.i("ID:", String.valueOf(id));

            fillCard();

        }


    }

    //сохранение состояние перед поворотом
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(BITMAP, article_bmp);
        outState.putString(JSON, new Gson().toJson(article));
        outState.putInt(ID, id);

    }

    //при паузе и при восстановлении меняю состоянии флага isAlive
    @Override
    protected void onPause() {
        super.onPause();
        isAlive = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isAlive = true;
    }

    @Override
    public void onBackPressed() {

        //перехватываю нажатие кнопки "Назад" и отправляю обратно Article и его id в списке родителя
        Intent intent = new Intent();
        intent.putExtra(ID, id);
        intent.putExtra(ARTICLE, new Gson().toJson(article));
        setResult(RESULT_OK, intent);
        //после завершаю текущее активити
        finish();

    }

    private void fillCard () {

        //проверка на наличе загруженнного изображения, при отсутствии загружает из сети

        if (article_bmp != null) {

            article_image.setImageBitmap(article_bmp);
            progressBar.setVisibility(View.INVISIBLE);

        } else {
            new GetImage(article_image).execute(article.getUrlToImage());
        }

        //запуск повторной загрузки изображение при клике на refresh

        refresh.setClickable(true);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetImage(article_image).execute(article.getUrlToImage());
            }
        });

        //проверка лайкнута ли статья и установка соответсвующего изображения
        if (article.isLike()) {

            img_like.setImageResource(R.drawable.ic_like);
            img_like_small.setImageResource(R.drawable.ic_like);

        }

        //установка слушателя кликов по значку лайка
        //по клику изменяюся изображения и количество лайков
        img_like.setClickable(true);
        img_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (article.isLike()) {
                    article.setLike(false);
                    img_like.setImageResource(R.drawable.ic_no_like);
                    img_like_small.setImageResource(R.drawable.ic_no_like);
                    likes_count.setText(String.valueOf(article.decreaseLikeCount()));
                } else {
                    article.setLike(true);
                    img_like.setImageResource(R.drawable.ic_like);
                    img_like_small.setImageResource(R.drawable.ic_like);
                    likes_count.setText(String.valueOf(article.increaseLikeCount()));
                }
            }
        });

        //заполнение текстовых полей
        author_text.setText(article.getAuthor());
        title_text.setText(article.getTitle());
        likes_count.setText(String.valueOf(article.getLikes_count()));
        data_text.setText(article.getPublishedAt());

        //увеличиваем количество текста для проверки фукции разворачивания
        String desc = article.getDescription();
        desc += " " + desc;
        desc += " " + desc;

        description_text.setText(desc);

        //установкаа слушателя для получения количества полей в описании
        //если полей больше 5 то текстовое поле ограничится 5ю полями и
        //добавится возможность развернуть его по нажатию на "more.."
        description_text.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                final int lines_count = description_text.getLineCount();

                if (lines_count > 5) {

                    description_text.setMaxLines(5);
                    more.setVisibility(View.VISIBLE);
                    more.setClickable(true);

                    more.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ObjectAnimator animation = ObjectAnimator.ofInt(
                                    description_text,
                                    "maxLines",
                                    lines_count);
                            animation.setDuration(500);
                            animation.start();
                            more.setVisibility(View.GONE);
                        }
                    });

                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    description_text.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {

                    description_text.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }

            }
        });

    }

    private class GetImage extends AsyncTask<String, Void, Bitmap> {

        ImageView image;

        public GetImage(ImageView image) {
            this.image = image;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            refresh.setVisibility(View.INVISIBLE);
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            if (isAlive) {
                Bitmap bitmap;

                try {
                    URL url = new URL(params[0]);
                    bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());

                    return bitmap;

                } catch (Exception e) {
                    return null;
                }
            } else {
                return null;
            }

        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);

            if (isAlive) {

                progressBar.setVisibility(View.INVISIBLE);

                if (bitmap != null) {

                    int width = bitmap.getWidth();
                    int height = bitmap.getHeight();

                    Log.i("GetImage:", String.format("Bitmap size is %d x %d", width, height));

                    //проверяем размер Bitmap и если надо, то делаем resize
                    if (width > 1024 || height > 768) {
                        bitmap = Bitmap.createScaledBitmap(bitmap, width/2, height/2, false);
                        Log.i("GetImage:", "bitmap resized");
                    }


                    article_bmp = bitmap;
                    image.setImageBitmap(article_bmp);
                } else {
                    refresh.setVisibility(View.VISIBLE);
                }

            }

        }
    }

}
