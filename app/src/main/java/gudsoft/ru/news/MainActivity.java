package gudsoft.ru.news;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.google.gson.*;
import gudsoft.ru.news.data.Article;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    LinearLayout container;
    ImageView refresh;
    ProgressBar progressBar;

    Context context = MainActivity.this;

    String news_api = "https://newsapi.org/v1/articles?source=google-news&sortBy=top&apiKey=0deda88a912147128058810433fd2bf7";

    //флаг для async_task
    boolean isAlive = true;
    ArrayList<Article> articles = new ArrayList<>();

    final String JSON = "json";
    final String ARTICLE = "article";
    final String ID = "id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        container = (LinearLayout) findViewById(R.id.container);
        progressBar = (ProgressBar) findViewById(R.id.progressBar_main);
        progressBar.setVisibility(View.INVISIBLE);

        //инициализирую изображение и делаю по-умолчанию невидимым
        //если из сети не подгрузится json - то делаю видимым
        //и ставлю обработчик нажатий, запускающий повторный запрос на сервер
        refresh = (ImageView) findViewById(R.id.refresh);
        refresh.setVisibility(View.INVISIBLE);
        refresh.setClickable(true);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new NewsTask().execute(news_api);
            }
        });

        //делаю проверку на наличие сохраненного сотояния и при наличии
        //выдергиваю строку json - лист Article и десириализую
        //при получении ошибки делаю новый запрос на сервер для обновления данных
        //если нет сохр сотояния, то просто делаю запрос на сервер
        if (savedInstanceState != null) {

            try {

                String json = savedInstanceState.getString(JSON);
                JsonElement element = new JsonParser().parse(json);
                JsonArray array = element.getAsJsonArray();

                //выкидывает ошибку при пустом массиве
                if (array.toString().equals("[]")) {
                    throw new Exception();
                }

                ArrayList<Article> list = new ArrayList<>();

                for (JsonElement e: array) {

                    list.add(new Article(e.toString()));

                }

                articles = list;
                fillContainer();

            } catch (Exception e) {
                new NewsTask().execute(news_api);
            }

        } else {
            new NewsTask().execute(news_api);
        }


    }

    //сохраняю состояние и сериализую ArrayList <Article> в строку json
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        String json = new Gson().toJson(articles);

        outState.putString(JSON, json);
        Log.i("Saved_articles:", json);

    }

    //в onPause & onResume меняю сотояние флага isAlive
    @Override
    protected void onPause() {
        super.onPause();
        isAlive = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isAlive = true;
    }

    private void fillContainer () {

        //очищаем таблицу от всех вьюшек
        container.removeAllViews();

        for (int i = 0; i < articles.size(); i++) {

            final Article article = articles.get(i);
            final int id = i;

            //инфлайтим ячейку и инициализируем ее view's
            RelativeLayout cell = (RelativeLayout) getLayoutInflater().inflate(R.layout.main_cell, null);
            TextView title = (TextView) cell.findViewById(R.id.title);
            ImageView img_like = (ImageView) cell.findViewById(R.id.img_like);

            title.setText(article.getTitle());

            //в случае isLike == true меняем изображение
            if (article.isLike()) {
                img_like.setImageResource(R.drawable.ic_like);
            }

            //делаю ячейку кликабельной и
            //по клику создаю интент и добавляю в него json строку выбранной Article из articles + её id в списке
            // запускаю активити с результатом
            cell.setClickable(true);
            cell.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toCard = new Intent(context, NewsCardActivity.class);
                    toCard.putExtra(ID, id);
                    toCard.putExtra(ARTICLE, new Gson().toJson(article));
                    startActivityForResult(toCard, 1);
                }
            });

            container.addView(cell);

        }



    }

    //Получаю результат с NewsCardActivity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {

            //получаю json строку article и десириализую её + id
            //листе articles по id  нахожу объект и заменяю его полученным article
            //пперерисовываю таблицу методм fillContainer

            int id = data.getIntExtra(ID, -1);
            Article article = new Article(data.getStringExtra(ARTICLE));

            if (id >= 0) {
                articles.set(id, article);
                fillContainer();
            }

        }

    }

    private class NewsTask extends AsyncTask<String, Void, ArrayList<Article>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            refresh.setVisibility(View.INVISIBLE);
        }

        @Override
        protected ArrayList<Article> doInBackground(String... params) {

            if (isAlive) {

                //для получения json с сервера использую библиотеку Jsoup
                try {
                    Connection connection = Jsoup.connect(params[0]);
                    connection.timeout(10000);
                    String json = connection.ignoreContentType(true).execute().body();
                    JsonElement element = new JsonParser().parse(json);
                    JsonObject resp = element.getAsJsonObject();
                    JsonArray array = resp.getAsJsonArray("articles");

                    ArrayList<Article> list = new ArrayList<>();

                    for (JsonElement e: array) {

                        list.add(new Article(e.toString()));

                    }

                    return list;

                } catch (Exception e) {
                    return  null;
                }

            } else {
                return null;
            }

        }

        @Override
        protected void onPostExecute(ArrayList<Article> list) {
            super.onPostExecute(list);

            if (isAlive) {

                progressBar.setVisibility(View.INVISIBLE);

                if (list != null) {
                    articles = list;
                    fillContainer();
                } else {
                    refresh.setVisibility(View.VISIBLE);
                }

            }

        }
    }

}
