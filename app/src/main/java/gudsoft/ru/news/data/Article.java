package gudsoft.ru.news.data;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Created by mb on 03.10.16.
 */
public class Article {

    private String author = "";
    private String title = "";
    private String description = "";
    private String url = "";
    private String urlToImage = "";
    private String publishedAt = "";
    private boolean isLike = false;
    private int likes_count = 100;

    public Article() {
    }

    public Article (String json) {

        JsonElement element = new JsonParser().parse(json);

        if (element != null) {
            JsonObject object = element.getAsJsonObject();

            this.author = object.get("author").isJsonNull() ? "No author" : object.get("author").getAsString();
            this.title = object.get("title").isJsonNull() ? "No title" : object.get("title").getAsString();
            this.description = object.get("description").isJsonNull() ? "No description" : object.get("description").getAsString();
            this.url = object.get("url").isJsonNull() ? "" : object.get("url").getAsString();
            this.urlToImage = object.get("urlToImage").isJsonNull() ? "" : object.get("urlToImage").getAsString();
            this.publishedAt = object.get("publishedAt").isJsonNull() ? "" : object.get("publishedAt").getAsString();
            this.isLike = object.get("isLike") == null ? false : object.get("isLike").getAsBoolean();
            this.likes_count = object.get("likes_count") == null ? 100 : object.get("likes_count").getAsInt();
        }

    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlToImage() {
        return urlToImage;
    }

    public void setUrlToImage(String urlToImage) {
        this.urlToImage = urlToImage;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public int getLikes_count() {
        return likes_count;
    }

    public int increaseLikeCount () {

        likes_count++;

        return likes_count;

    }

    public int decreaseLikeCount () {

        likes_count --;

        return likes_count;

    }
}
